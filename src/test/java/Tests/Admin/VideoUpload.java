package Tests.Admin;

import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static junit.framework.Assert.assertEquals;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utilities.DriverManager;

/**
 *
 * @author sergey
 */
public class VideoUpload {

    public DriverManager drvs;
    Login objTestLogin = new Login();

    WebDriver driver;

    Pages.Login objHomePage;

    Pages.VideoUpload objVU;

    Pages.VideoOverview objOver;

    protected String child;
    protected String strDate;
    protected String strTitleUp = "Title test";
    protected String strDescriptionUp = "Test Description";
    protected String strTitleDown = "Edit Title test ";
    protected String strDescriptionDown = "Edit Test Description";

    private static final String urlParamsUpload = "index.php/video_upload/general/";

    private static final String urlParamsOverview = "index.php/gallery_video/overview/";

    private VideoUpload() throws Exception {
        System.out.println("Satrt - logaut");

        objTestLogin.BeforeTest();
        objTestLogin.Start();
        objTestLogin.testLoginform();

        this.driver = driver = getWebDriver();
        this.objVU = new Pages.VideoUpload(driver);

    }

    /**
     * Page object video upload
     */
    @BeforeTest
    public void BeforeTest() throws Exception {

    }

    @AfterTest
    public void tearDown() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    @Test
    public void oupen_overview() throws InterruptedException {
        System.out.println("oupen overview");
        open(utilities.DefaultParams.BseURL + objOver.urlParamsOverview);
//         Thread.sleep(1000);
//        Pages.VideoOverview.click_a_link_child_video_upload();

    }

    @Test(priority = 1)
    public void Test_adding_video_file() throws InterruptedException, Exception {
        System.out.println("Test_adding_video_file");
        open(utilities.DefaultParams.BseURL + urlParamsUpload + utilities.DefaultParams.testChild);
        objVU.addVideosFile("test_video.mp4");

    }

    @Test(priority = 2)
    public void Test_Check_Uploaded_Video_Template() throws InterruptedException {
        System.out.println("CheckUploadedVideoTemplate");
        assertEquals(objVU.CheckUploadedVideoTemplate(), true);
    }

    @Test(priority = 3)
    public void Test_upload_general_videoData_piker() throws InterruptedException {
        System.out.println("Test_upload_general_videoData_piker");
        objVU.chengeDate();
        this.strDate = objVU.getDateuploaded();

    }

    @Test(priority = 4)
    public void Test_upload_general_videoData_meta() throws InterruptedException {
        System.out.println("Test_upload_general_videoData_meta");
        objVU.setTitleUp(strTitleUp);
        objVU.setDescriptionUp(strDescriptionUp);

    }

    @Test(priority = 5)
    public void Test_upload_general_video() throws InterruptedException {
        System.out.println("Test_upload_general_video");
        objVU.clickUpload();

    }

    @Test(priority = 6)
    public void Test_check_download_video_template() throws InterruptedException {
        System.out.println("Test_check_download_video_template");
        objVU.CheckDownloadVideoTemplate();
    }

    @Test(priority = 7)
    public void Test_check_meta() throws InterruptedException {
        System.out.println("Test_check_meta");
        Configuration.collectionsTimeout = 4;

        assertEquals(strDate, objVU.getDateuploaded());
        assertEquals(strTitleUp, objVU.getTitleDown());
        assertEquals(strDescriptionUp, objVU.getDescriptionDown());

    }

    @Test(priority = 8)
    public void Test_check_video_encoding() throws InterruptedException {
        System.out.println("Test_check_video_encoding");
        objVU.doneProgressEncodingUploadVideo();
    }

    @Test(priority = 9)
    public void Test_edit_metadata_to_the_video_template() throws InterruptedException {
        System.out.println("Test_edit_metadata_to_the_video_template");
        objVU.setTitleDown(strTitleDown);
        objVU.setDescriptionDown(strDescriptionDown);

        assertEquals(strDate, objVU.getDateuploaded());
        assertEquals(strTitleDown, objVU.getTitleDown());
        assertEquals(strDescriptionDown, objVU.getDescriptionDown());
    }

    @Test(priority = 10)
    public void Test_delete_video_file() throws InterruptedException {
        System.out.println("Test_delete_video_file");
        objVU.clickADelete();
    }

    @Test(priority = 13)
    public void Test_upload_file_video11() throws InterruptedException, Exception {
        objVU.addVideosFile("video11.webm");
        System.out.println("Test_delete_video_file video11.webm");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 14)
    public void Test_upload_file_video16() throws InterruptedException, Exception {
        objVU.addVideosFile("video16.swf");
        System.out.println("Test_delete_video_file video16.swf");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 15)
    public void Test_upload_file_video4() throws InterruptedException, Exception {
        objVU.addVideosFile("video4.3gp");
        System.out.println("Test_delete_video_file video4.3gp");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 16)
    public void Test_upload_file_video9() throws InterruptedException, Exception {
        objVU.addVideosFile("video9.ogv");
        System.out.println("Test_delete_video_file video9.ogv");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 17)
    public void Test_upload_file_image2() throws InterruptedException, Exception {
        objVU.addVideosFile("image2.gif");
        System.out.println("Test_delete_video_file image2.gif");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 18)
    public void Test_upload_file_video12() throws InterruptedException, Exception {
        objVU.addVideosFile("video12.wmv");
        System.out.println("Test_delete_video_file video12.wmv");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 19)
    public void Test_upload_file_video5() throws InterruptedException, Exception {
        objVU.addVideosFile("video5.3g2");
        System.out.println("Test_delete_video_file video5.3g2");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

   

//    @Test(priority = 21)
//    public void Test_upload_file_video13() throws InterruptedException, Exception {
//        objVU.addVideosFile("video13.m2ts");
//        System.out.println("Test_delete_video_file video13.m2ts");
//        objVU.clickUpload();
//        objVU.CheckDownloadVideoTemplate();
//        objVU.doneProgressEncodingUploadVideo();
//        objVU.clickADelete();
//    }

    @Test(priority = 22)
    public void Test_upload_file_video1() throws InterruptedException, Exception {
        objVU.addVideosFile("video1.mov");
        System.out.println("Test_delete_video_file video1.mov");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 23)
    public void Test_upload_file_video6() throws InterruptedException, Exception {
        objVU.addVideosFile("video6.mpg");
        System.out.println("Test_delete_video_file video6.mpg");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 24)
    public void Test_upload_file_video14() throws InterruptedException, Exception {
        objVU.addVideosFile("video14.mkv");
        System.out.println("Test_delete_video_file video14.mkv");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 25)
    public void Test_upload_file_video7() throws InterruptedException, Exception {
        objVU.addVideosFile("video7.divx");
        System.out.println("Test_delete_video_file video7.divx");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 26)
    public void Test_upload_file_video10() throws InterruptedException, Exception {
        objVU.addVideosFile("video10.rmvb");
        System.out.println("Test_delete_video_file video10.rmvb");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

    @Test(priority = 27)
    public void Test_upload_file_video15() throws InterruptedException, Exception {
        objVU.addVideosFile("video15.mts");
        System.out.println("Test_delete_video_file video15.mts");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }
//
//    @Test(priority = 28)
//    public void Test_upload_file_video3() throws InterruptedException, Exception {
//        objVU.addVideosFile("video3.avi");
//        System.out.println("Test_delete_video_file video3.avi");
//        objVU.clickUpload();
//        objVU.CheckDownloadVideoTemplate();
//        objVU.doneProgressEncodingUploadVideo();
//        objVU.clickADelete();
//    }

    @Test(priority = 29)
    public void Test_upload_file_video8() throws InterruptedException, Exception {
        objVU.addVideosFile("video8.flv");
        System.out.println("Test_delete_video_file video8.flv");
        objVU.clickUpload();
        objVU.CheckDownloadVideoTemplate();
        objVU.doneProgressEncodingUploadVideo();
        objVU.clickADelete();
    }

}
