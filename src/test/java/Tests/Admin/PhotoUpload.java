/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests.Admin;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utilities.DriverManager;

/**
 *
 * @author sergey
 */
public class PhotoUpload {

    public DriverManager drvs;
    Login objTestLogin = new Login();

    WebDriver driver;

    Pages.Login objHomePage;

    private Pages.PhotoUpload objPU;

    private static final String urlParamsUpload = "index.php/gallery/upload/photo/";

    private static final String urlParamsOverview = "index.php/gallery/overview";

    protected String child;
    protected String strDate;
    protected String strTitleUp = "Title test";
    protected String strDescriptionUp = "Test Description";
    protected String strTitleDown = "Edit Title test ";
    protected String strDescriptionDown = "Edit Test Description";

    private PhotoUpload() throws Exception {

    }

    /**
     * Page object video upload
     */
    @BeforeTest
    public void BeforeTest() throws Exception {
        System.out.println("Satrt - logaut");

        objTestLogin.BeforeTest();
        objTestLogin.Start();
        objTestLogin.testLoginform();

        this.driver = driver = getWebDriver();
        this.objPU = new Pages.PhotoUpload(driver);

        open(utilities.DefaultParams.BseURL + urlParamsOverview);

        open(utilities.DefaultParams.BseURL + urlParamsUpload + utilities.DefaultParams.testChild);
    }

    @AfterTest
    public void Test_tearDown() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    @Test(priority = 1)
    public void Test_addPHoto() throws InterruptedException, Exception {
        System.out.println("oupen overview");
        objPU.addPhotoFile("image1.png");
    }

    @Test(priority = 2)
    public void Test_checkAddPhoto() {
        System.out.println("checkAddPhoto");
        objPU.checkAddPhoto();
    }

    @Test(priority = 3)
    public void Test_photoRotateLeft() {
        System.out.println("photoRotateLeft");
        objPU.bd__action__photoRotateLeft();
        objPU.check_update_status();
    }

    @Test(priority = 4)
    public void Test_photoRotateRight() {
        System.out.println("photoRotateRight");
        objPU.bd__action__photoRotateRight();
        objPU.check_update_status();
    }

    @Test(priority = 5)
    public void Test_set_photo_title() {
        System.out.println("set_photo_title");
        objPU.set_photoTitle(strTitleUp);
        objPU.check_update_status();
    }

    @Test(priority = 6)
    public void Test_set_photo_Description() {
        System.out.println("set_photo_Description");
        objPU.set_photoDescription(strDescriptionUp);
        objPU.check_update_status();
    }

    @Test(priority = 6)
    public void Test_photoDate() {
        System.out.println("Test_photoDate");
        objPU.photoDate();
        objPU.check_update_status();
    }

  

    @Test(priority = 7)
    public void Test_toggleEyoslist() {
        System.out.println("Test_Eyoslis");
        objPU.toggleEyoslist();

    }
    
   
    @Test(priority = 8)
    public void Test_clickEyos() throws InterruptedException {
        System.out.println("Test_clickEyos");
        objPU.clickEyosType();
        objPU.clickEyosTab();
        objPU.clickEyosItem();
         Thread.sleep(500);

    }
    @Test(priority = 9)
    public void Test_delete_photo() throws InterruptedException {
        System.out.println("delete_photo");
        objPU.photoDelete();

    }
    @Test(priority = 10)
    public void Test_AddPhoto_2() throws Exception {
        System.out.println("Test_AddPhoto_2");
        objPU.addPhotoFile("image2.gif");
        objPU.checkAddPhoto();
        objPU.photoDelete();
    }

    @Test(priority = 11)
    public void Test_AddPhoto_3() throws Exception {
        System.out.println("Test_AddPhoto_2");
        objPU.addPhotoFile("maxresdefault.jpg");
        objPU.checkAddPhoto();
        objPU.photoDelete();
    }

}
