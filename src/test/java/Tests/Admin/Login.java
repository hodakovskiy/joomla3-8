/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests.Admin;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utilities.DriverManager;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.open;
import junit.framework.Assert;
/**
 *
 * @author sergey
 */
public class Login  {

    Pages.Login objHomePage;
    public DriverManager drvs;
 
    
    public static final String urlParams = "index.php/user/home/";
     
    @BeforeTest
    public void BeforeTest() throws Exception {
       this.drvs =   new DriverManager("chrome");
       Configuration.collectionsTimeout = 8000;
    
    }

     @Test(priority = 0)
    public void Start() throws Exception {
       System.out.println("Start Login " + utilities.DefaultParams.BseURL + urlParams);
       open(utilities.DefaultParams.BseURL + urlParams);
        
    }

    @Test(priority = 1)
    public void testLoginform() throws Exception {
        timeout = 1;
        objHomePage.setUserName(utilities.DefaultParams.loginAdmin);
         System.out.println("login to application ");
        //	login to application
        objHomePage.setPassword(utilities.DefaultParams.loginPass);
        // go the next page
        objHomePage.clickLogin();
  
    }
    
     /**
   * This test case will login Login to application Verify the home page using
   * Dashboard message
   */
  @Test(priority = 2)
  public void test_Home_Page_Appear_Correct() {
       System.out.println("Verify home page");
       Assert.assertTrue(objHomePage.checkLoadingPage());
   
  }

    @AfterTest
    public void tearDown() throws InterruptedException {
        Thread.sleep(300);
        drvs.closeDriver();
    }

 
 
}
