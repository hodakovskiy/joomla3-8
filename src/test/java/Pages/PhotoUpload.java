/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pages;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.hasClass;
import static com.codeborne.selenide.Condition.hasText;
import static com.codeborne.selenide.Condition.not;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utilities.DefaultParams;

/**
 *
 * @author sergey
 */
public class PhotoUpload {

    WebDriver driver;

    DefaultParams DParam;

    public PhotoUpload(WebDriver driver) {
        Configuration.timeout = 600;
        this.DParam = new DefaultParams();
        this.driver = getWebDriver();
    }

    /**
     * Add test video file
     *
     */
    public void addPhotoFile(String filename) throws Exception {
        Thread.sleep(500);
        String Filename = DParam.testFilesDir + filename;
        System.out.println("Satrt Add file " + Filename);
        if (!new File(Filename).exists()) {
            driver.quit();
            throw new Exception("This file (" + Filename + ") does not exist");
        }
        File file = new File(utilities.DefaultParams.testFilesDir + filename);
        $(".bd__uploader__input").uploadFile(file);
        System.out.println("End Add file");
    }

    public void check_update_status() {
        $(".thumbnail_status").waitUntil(hasClass("inProcess"), 15000);
        $(".thumbnail_status").waitUntil(hasClass("success"), 15000);
        $(".thumbnail_status").waitUntil(hasClass("hidden"), 15000);
    }

    @SuppressWarnings("empty-statement")
    public void checkAddPhoto() {
        $(".bd__photo_uploaded").shouldBe(enabled);
    }

    public void bd__action__photoRotateLeft() {
        $(".bd__action__photoRotateLeft").shouldBe(enabled).click();
    }

    public void bd__action__photoRotateRight() {
        $(".bd__action__photoRotateRight").shouldBe(enabled).click();
    }

    public  void photoDate() {
        $(".bd__field__photoDate").shouldBe(enabled).click();
        $(".active .dey").shouldBe(enabled).click();
    }
    
    public void set_photoTitle(String strTitle) {
        $(".bd__field__photoTitle").clear();
        $(".bd__field__photoTitle").sendKeys(strTitle);
    }

    public void set_photoDescription(String strDescription) {
        $(".bd__field__photoDescription").clear();
        $(".bd__field__photoDescription").sendKeys(strDescription);
    }

    public void photoDelete() throws InterruptedException {
        By buttonDelete = By.xpath("/html/body/div[6]/div[7]/div/button");

        $(".bd__action__photoDelete").shouldBe(enabled).click();
        Thread.sleep(1000);
        $(buttonDelete).click();

        $("body").click();

        $(".bd__photo_uploaded").waitUntil(not(hasClass("bd__photo_uploaded")), 1500);

    }
    
    public void toggleEyoslist () {
        $(".bd__action__toggleEyoslist").shouldBe(enabled).click();
        $(".bd__photo_uploaded").waitUntil(hasClass("bd__container__eyosSmall"), 500);
        
    }
    
    public void clickEyosType () throws InterruptedException {
        Thread.sleep(500);
       $(".bd__select_type").shouldBe(enabled).click();
       $(byText("EAD")).shouldBe(enabled).click();
        
    }
    
     public void clickEyosTab () {
       $(".bd__select_tab").shouldBe(enabled).click();
       $(byText("Exploring and using media and materials")).shouldBe(enabled).click();
        
    }
      public void clickEyosItem () {
       $(".bd__select_item").shouldBe(enabled).click();
       $(byText("(8m to 20m) - Move their whole bodies to sounds they enjoy, such as music or a regular beat.")).shouldBe(enabled).click();
        
    }
}
