package Pages;

import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

/**
 *
 * @author sergey
 */
public class Login {

    //Set user name in textbox
    public static SelenideElement setUserName(String strUserName) {
        return $(By.id("username")).val(strUserName);

    }

    //Set password in password textbox
    public static SelenideElement setPassword(String strPassword) {
        return $(By.id("passwd")).val(strPassword);
    }

    //Click on login button
    public static Login clickLogin() {
        $(By.tagName("button")).pressEnter();
        return page(Login.class);
    }

    public static boolean checkLoadingPage() {
        Configuration.collectionsTimeout = 8000;
        try {
            $(By.className(".icon-logout"));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void setUserName1(String loginAdmin) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
