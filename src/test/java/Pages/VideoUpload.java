package Pages;

import static com.codeborne.selenide.Condition.disappears;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Condition.not;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.$;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utilities.DefaultParams;

/**
 *
 * @author sergey
 */
public class VideoUpload {

    WebDriver driver;
    
    DefaultParams DParam;

    /**
     * Generall video upload page
     */
    By generalPageUpload = By.id("bd-container__uploader");

    /**
     * Button bar - start video upload all file
     */
    By startUpload = By.cssSelector("button.start");
    /**
     * input type file
     */
    By videosFile = By.id("videosFile");

    /**
     * Add Video template
     */
    By videoUploadTemplete = By.className("box-template-video");

    /**
     * Template After downloading the video.
     */
    By videoDounloadTemplete = By.className("template-download");
    /**
     * Button bar - cancel video upload, Canceling the uploading Process
     */
    By buttonCancel = By.cssSelector("button.cancel");
    /**
     * Close link-button video, cancel video upload
     */
    By aCancel = By.cssSelector("a.close");
    /**
     * delete link-button video
     */
    By aDelete = By.cssSelector("a.delete");
    /**
     * Date upload file
     */
    By date = By.cssSelector("div.date");
    /**
     * Any date on the calendar
     */
    By calendar = By.cssSelector("td.active");
    /**
     * Hidden field with date format YY-dd-mm
     */
    By dateInput = By.cssSelector("#fileupload input.dateuploaded");

    /**
     * The title of the video that start for upload
     */
    By titleUp = By.name("title");
    /**
     * Video title after download
     */
    By titleDown = By.className("bd__field__photoTitle");
    /**
     * The description of the video that start for upload
     */
    By descriptionUp = By.name("videodesc");
    /**
     * Video description after download
     */
    By descriptionDown = By.cssSelector("#videodesc");
    /**
     * Button open box Early Years Outcomes.
     */
    By buttonOpenEYO = By.className("video-duplicate-progress");
    /**
     * public void public void public void
     */
    By buttonAddEYO = By.cssSelector("#fileupload .bd__action__addEyo");
    /**
     * Select AOL | tab
     */
    By selectAOL = By.cssSelector("#fileupload .bd__select_tab");
    /**
     * Select Aspect | select tab
     */
    By selectAspect = By.cssSelector("#fileupload .bd__select_tab");
    /**
     * Select Early Years Outcome:
     */
    By selectEYO = By.cssSelector("#fileupload .bd__select_item");
    /**
     * Click to delete this outcome.
     */
    By deleteEYO = By.cssSelector("#fileupload .bd__action__deleteEyo");

    By progressUploadStatus = By.className("progress-text-all");

    By progressEncodingUploadStatus = By.className("chart-video");

    By statusErrorUpload = By.className("chart-video");

    public VideoUpload(WebDriver driver) {
        this.DParam = new DefaultParams();

        this.driver = getWebDriver();

    }

    /**
     * Check go to the main video page
     */
    public boolean checkGeneralHome() {
        boolean present;
        try {
            driver.findElement(generalPageUpload);
            present = true;
        } catch (NoSuchElementException e) {
            present = false;
        }
        return present;
    }

    /**
     * Add test video file
     *
     */
    public void addVideosFile(String filename) throws Exception {
         Thread.sleep(500);
        String Filename = DParam.testFilesDir + filename;
        System.out.println("Satrt Add file " + Filename);
        if (!new File(Filename).exists()) {
            driver.quit();
            throw new Exception("This file (" + Filename + ") does not exist");
        }
        File file = new File(utilities.DefaultParams.testFilesDir + filename);
        $("#videosFile").uploadFile(file);
        System.out.println("End Add file");
    }

    /**
     * Check add video template
     *
     * @return boolean
     */
    public boolean CheckUploadedVideoTemplate() {
        try {
           $(videoUploadTemplete).waitUntil(enabled, 1000);;
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Check add video template
     *
     * @return boolean
     * @throws java.lang.InterruptedException
     */
    public boolean CheckDownloadVideoTemplate() throws InterruptedException {
        try {
            $(videoDounloadTemplete).waitUntil(enabled, 500000);;
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

     public void clickUploadError() {
         $(".error ").waitUntil(not(matchText("^.$")), 2000);
     }
    /**
     * Start upload video file
     */
    public void clickUpload() throws InterruptedException {
        System.out.println("Click Uploadfile");

        WebElement element = driver.findElement(startUpload);
        $("body").click();
         Thread.sleep(500);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
        driver.findElement(startUpload).click();
    }

    /**
     * Click Button Cancel is Button bar
     */
    public void clickButtonCancel() {
        System.out.println("Click Button Cancel");
        driver.findElement(buttonCancel).click();
    }

    /**
     * Click a link Close Card video, cancel video upload
     */
    public void clickACancel() {
        System.out.println("Click Button Cancel");
        $("body").click();
        driver.findElement(aCancel).click();
    }

    /**
     * Click the Cross button and delete the video.
     */
    public void clickADelete() {
        $("body").click();
        driver.findElement(aDelete).click();
    }

    /**
     * Open date on the calendar
     */
    public void clickDate() throws InterruptedException {
        $("body").click();
        $(date).click();
    }

    /**
     * Change Date to Select a random date Click on the date in the calendar
     *
     * @throws java.lang.InterruptedException
     */
    public void chengeDate() throws InterruptedException {
        System.out.println("Click date calendar");

        $(date).click();
        Thread.sleep(500);
        $(calendar).click();

    }

    /**
     * Get Date uploaded format YY-dd-mm
     */
    public String getDateuploaded() {
        String attribute = $(dateInput).val();
        return attribute;
    }

    /**
     * Set the title of the video in the text box so that before starting the
     * upload
     *
     * @param strTitle
     */
    public void setTitleUp(String strTitle) {
        driver.findElement(titleUp).sendKeys(strTitle);
    }

    /**
     * Set the title of the video in the text box after downloading
     *
     * @param strTitleDown
     */
    public void setTitleDown(String strTitleDown) {
        $(titleDown).clear();
        driver.findElement(titleDown).sendKeys(strTitleDown);
    }

    /**
     * Get the title of the video in the text box after downloading
     *
     * @param strTitleDown
     */
    public String getTitleDown() {

        String title = $(titleDown).val();
        return title;
    }

    /**
     * Set the description of the video in the text box so that before starting
     * the upload
     *
     * @param strDescription
     */
    public void setDescriptionUp(String strDescription) {
        driver.findElement(descriptionUp).sendKeys(strDescription);
    }

    /**
     * Set Video description after download
     *
     * @param strDescription
     */
    public void setDescriptionDown(String strDescription) {
        $(descriptionDown).clear();
        driver.findElement(descriptionDown).sendKeys(strDescription);
    }

    /**
     * Set Video description after download
     *
     * @param strDescription
     */
    public String getDescriptionDown() {
        String description = $(descriptionDown).val();
        System.out.println("getDescriptionDown " + description);
        return description;
    }

    /**
     * Click Button open box Early Years Outcomes.
     */
    public void clickButtonOpenEYO() {
        driver.findElement(buttonOpenEYO).click();
    }

    /**
     * Click Button Add Early Years Outcomes
     */
    void clickButtonAddEYO() throws InterruptedException {
        Thread.sleep(400);
        driver.findElement(buttonAddEYO).click();
    }

    public void selectAOL() {
        driver.findElement(selectAOL).click();
    }

    /**
     * The wait for the video download process returns positively if the video
     * is loaded negative if the download error
     *
     * @return boolean
     */
    public SelenideElement doneUploadStatus() throws InterruptedException {

        System.out.println("start doneUploadStatus");

        return $(".click-start-msg").waitUntil(disappears, 2);

    }

    /**
     * The wait for the video download process returns positively if the video
     * is loaded negative if the download error
     *
     * @return boolean
     */
    public boolean doneProgressEncodingUploadVideo() throws InterruptedException {
        System.out.println("Start doneProgressEncodingUploadVideo ");
        boolean status = false;
        long startTime = System.currentTimeMillis();
        try {
            while (status == false) {
                Thread.sleep(1000);
                status = checkStatusPorgress(driver.findElement(progressEncodingUploadStatus).getAttribute("title"));
                if ((System.currentTimeMillis() - startTime) >= 120000) {

                    return false;
                } else {

                }
            }

            return status;
        } catch (NoSuchElementException e) {

            return false;

        }

    }

    /**
     * Check status video upload
     *
     * @return boolean
     */
    public boolean CheckStatusErrorUpload() {
        System.out.println("CheckStatusErrorUpload ");

        boolean present;
        Configuration.collectionsTimeout = 400;
        try {

            $("#fileupload").findElement(statusErrorUpload);
            present = false;
        } catch (NoSuchElementException e) {
            present = true;
        }
        return present;
    }

    public static boolean checkStatusPorgress(String str) {
        System.out.println("checkStatusPorgress ");
        Pattern p = Pattern.compile("100");
        Matcher m = p.matcher(str);

        return m.find();
    }

}
