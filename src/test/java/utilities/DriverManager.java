/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import static com.codeborne.selenide.Configuration.startMaximized;
import com.codeborne.selenide.WebDriverRunner;
import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author sergey
 */
public class DriverManager {

    public static WebDriver driver = null;

    MainApp objOS;

    /**
     *
     * @param browser firefox , chrome , Edge
     * @return
     * @throws Exception
     */
    public DriverManager(String browser) throws Exception {
        if( driver != null) {
            return ;
        } 
        this.objOS = new MainApp();
        startMaximized = true;
        System.out.println("Drivers initiated " + browser);

        //Check if parameter passed from TestNG is 'firefox'
        if (browser.equalsIgnoreCase("firefox")) {
            //create firefox instance
//            System.setProperty("webdriver.firefox.marionette", this.getFileDriver("geckodriver"));
            driver = new FirefoxDriver();
        } //Check if parameter passed as 'chrome'
        else if (browser.equalsIgnoreCase("chrome")) {
            //set path to chromedriver.exe
            System.setProperty("webdriver.chrome.driver", this.getFileDriver("chromedriver"));
            //create chrome instance
            driver = new ChromeDriver();
        } //Check if parameter passed as 'Edge'
        else if (browser.equalsIgnoreCase("Edge")) {
            //set path to Edge.exe
            System.setProperty("webdriver.edge.driver", this.getFileDriver("MicrosoftWebDriver.exe"));
            //create Edge instance
            driver = new EdgeDriver();
        } else {
            //If no browser passed throw exception
            throw new Exception("Browser is not correct");
        }
        WebDriverRunner.setWebDriver(driver);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
         DriverManager.driver = driver;
       
    }

    public void closeDriver() {
        if (driver != null) {
            driver.close();
        }

    }

    /**
     *
     * @param brouser
     * @return drivsFilename
     * @throws Exception $TODO add patch wim anf mak web driver
     */
    private String getFileDriver(String brouser) throws Exception {

        System.out.println("It's name is: " + objOS.OsName);
        System.out.println("It's arch is: " + objOS.OsArchitecture);
        String drivsFilename = null;

        if ("Windows".equals(objOS.OsName)) {
            drivsFilename = ".\\include\\drivers\\Windows\\" + brouser + ".exe";

        } else if ("Linux".equals(objOS.OsName)) {
            drivsFilename = "./include/drivers/Linux/" + brouser;
        } else if ("Mac".equals(objOS.OsName)) {
            drivsFilename = "/include/drivers/Mac/" + brouser;
        } else {
            System.out.println("Browser is not correct: " + objOS.OsName);
            throw new Exception("Browser is not correct");
        }

        System.out.println("It's name is: " + drivsFilename);

        if (!new File(drivsFilename).exists()) {
            System.out.println("This driver (" + drivsFilename + ") does not exist");
            throw new Exception("This driver (" + drivsFilename + ") does not exist");
        } else {
            return drivsFilename;
        }

    }

}
